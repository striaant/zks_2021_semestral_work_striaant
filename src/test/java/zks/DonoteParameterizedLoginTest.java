package zks;

import cz.cvut.fel.still.sqa.seleniumStarterPack.config.DriverFactory;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import zks.tasks.LoginDifferentCombinations;
import zks.tasks.StartsWith;

import java.io.IOException;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertTrue;
import static zks.pageObjects.DonoteHomePage.INPUT_REQUIRED;
import static zks.questions.TodoErrorMessage.theDisplayTodoErrorMessage;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom(value = "src/test/resources/DONOTE-output.csv")
public class DonoteParameterizedLoginTest {
    private String username;
    private String password;
    private String todo;
    private WebDriver browser;
    Actor anton  = Actor.named("Anton");

    @Before
    public void before() throws IOException {
        browser = new DriverFactory().getDriver();
        givenThat(anton).can(BrowseTheWeb.with(browser));
    }

    @After
    public void closeBrowser() {
        browser.close();
    }

    public void setUsername(String username) {
        if (username.equals("...")){
            this.username = "";
        }
        else{
            this.username = username;
        }
    }

    public void setPassword(String password) {
        if (password.equals("...")){
            this.password = "";
        }
        else{
            this.password = password;
        }
    }

    public void setTodo(String todo) {
        if (todo.equals("...")){
            this.todo = "";
        }
        else{
            this.todo = todo;
        }
    }
    @Test
    public void loginDifferentCombinations(){
        givenThat(anton).wasAbleTo(StartsWith.donoteHomePage());
        when(anton).attemptsTo(LoginDifferentCombinations.called(this.username,this.password, this.todo));
        if(!this.password.equals("") && !this.username.equals("")){
            String error = "Todo contained empty string";
            then(anton).should(seeThat(theDisplayTodoErrorMessage(),hasItem(error)));
        }else{
            System.out.println(this.password.equals(""));
            assertTrue(INPUT_REQUIRED.resolveFor(anton).isVisible());
        }
    }
}
