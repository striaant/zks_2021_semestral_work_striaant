package zks.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static zks.pageObjects.DonoteHomePage.*;

public class CreateNewUserWithoutNumbersInPassword implements Task {

    public static CreateNewUserWithoutNumbersInPassword called() {
        return Instrumented.instanceOf(CreateNewUserWithoutNumbersInPassword.class).withProperties();
    }


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(REQISTRATION_BUTTON),
                Enter.theValue("hentai49").into(REQISTRATION_LOGIN_INPUT),
                Enter.theValue("qwerty").into(REQISTRATION_PASSWORD_INPUT),
                Enter.theValue("qwerty").into(REQISTRATION_DUPLICATE_PASSWORD_INPUT),
                Click.on(REQISTRATION_SUBMIT_BUTTON)
        );
    }
}
