package zks.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static zks.pageObjects.DonoteHomePage.*;

public class LoginDifferentCombinations implements Task {

    String username;
    String password;
    String todo;

    public LoginDifferentCombinations(String username, String password, String todo){
        this.username = username;
        this.password = password;
        this.todo = todo;
    }

    public static LoginDifferentCombinations called(String username, String password, String todo) {
        return Instrumented.instanceOf(LoginDifferentCombinations.class).withProperties(username, password, todo);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        if(this.username.equals("")||this.password.equals("")){
           actor.attemptsTo(
                   Enter.theValue(this.username).into(LOGIN_INPUT_FIELD),
                   Enter.theValue(this.password).into(PASSWORD_INPUT_FIELD),
                   Click.on(SUBMIT_LOGIN_BUTTON)
           );
       }else{
           actor.attemptsTo(
                   Enter.theValue(this.username).into(LOGIN_INPUT_FIELD),
                   Enter.theValue(this.password).into(PASSWORD_INPUT_FIELD),
                   Click.on(SUBMIT_LOGIN_BUTTON),
                   Click.on(NAVBAR_DROPDOWN),
                   Click.on(TODO_SECTION),
                   Enter.theValue(this.todo).into(TODO_INPUT_FIELD),
                   Click.on(TODO_SUBMIT_BUTTON)
           );
       }
    }
}
