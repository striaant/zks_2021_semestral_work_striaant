package zks.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;

import static zks.pageObjects.DonoteHomePage.*;

public class CreateNewUserAccount implements Task {

    public static CreateNewUserAccount called() {
        return Instrumented.instanceOf(CreateNewUserAccount.class).withProperties();
    }

    @Step("{0} is category visible")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(REQISTRATION_BUTTON),
                Enter.theValue("admin3").into(REQISTRATION_LOGIN_INPUT),
                Enter.theValue("qwerty1").into(REQISTRATION_PASSWORD_INPUT),
                Enter.theValue("qwerty1").into(REQISTRATION_DUPLICATE_PASSWORD_INPUT),
                Click.on(REQISTRATION_SUBMIT_BUTTON)
        );
    }
}
