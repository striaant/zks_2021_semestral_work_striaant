package zks.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static zks.pageObjects.DonoteHomePage.*;

public class Tdl3 implements Task {

    public static Tdl3 called() {
        return Instrumented.instanceOf(Tdl3.class).withProperties();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue("admin3").into(LOGIN_INPUT_FIELD),
                Enter.theValue("qwerty1").into(PASSWORD_INPUT_FIELD),
                Click.on(SUBMIT_LOGIN_BUTTON),
                Click.on(NAVBAR_DROPDOWN),
                Click.on(LOGOUT_BUTTON)
        );
    }
}
