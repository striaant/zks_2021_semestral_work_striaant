package zks.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static zks.pageObjects.DonoteHomePage.*;

public class CreateUserWithEmptyPasswords implements Task {

    public static CreateUserWithEmptyPasswords called() {
        return Instrumented.instanceOf(CreateUserWithEmptyPasswords.class).withProperties();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(REQISTRATION_BUTTON),
                Enter.theValue("hentai47").into(REQISTRATION_LOGIN_INPUT),
                Click.on(REQISTRATION_SUBMIT_BUTTON)
        );
    }
}
