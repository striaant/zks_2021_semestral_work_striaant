package zks.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static zks.pageObjects.DonoteHomePage.*;

public class CreateEmptyUser implements Task {

    public static CreateEmptyUser called() {
        return Instrumented.instanceOf(CreateEmptyUser.class).withProperties();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(REQISTRATION_BUTTON),
                Click.on(REQISTRATION_SUBMIT_BUTTON)
        );
    }
}
