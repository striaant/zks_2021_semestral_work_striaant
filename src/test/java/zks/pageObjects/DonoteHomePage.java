package zks.pageObjects;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import net.serenitybdd.core.pages.PageObject;

@DefaultUrl("https://zks-front.herokuapp.com/")
public class DonoteHomePage extends PageObject {

    public static final Target LOGIN_INPUT_FIELD = Target
            .the("Login input field")
            .locatedBy("//*[@id=\"login\"]");

    public static final Target PASSWORD_INPUT_FIELD = Target
            .the("Password input field")
            .locatedBy("//*[@id=\"password\"]");

    public static final Target SUBMIT_LOGIN_BUTTON = Target
            .the("Submit login data button")
            .locatedBy("//*[@id=\"do_login\"]");

    public static final Target NAVBAR_DROPDOWN = Target
            .the("Navbar dropdown")
            .locatedBy("//*[@id=\"navbarDropdown\"]");

    public static final Target TODO_SECTION = Target
            .the("Donote todo section")
            .locatedBy("//*[@id=\"navbarTogglerDemo02\"]/ul/li[2]/div/a[1]");

    public static final Target TODO_INPUT_FIELD = Target
            .the("Donote todo input field")
            .locatedBy("//*[@id=\"myInput\"]");

    public static final Target TODO_SUBMIT_BUTTON = Target
            .the("Todo submit button")
            .locatedBy("//*[@id=\"btn\"]");

    public static final Target REQISTRATION_BUTTON = Target
            .the("Registration button")
            .locatedBy("/html/body/div/div/div[1]/fieldset/form/a");

    public static final Target REQISTRATION_LOGIN_INPUT = Target
            .the("Registration login input field")
            .locatedBy("//*[@id=\"form\"]/label[1]/input");

    public static final Target REQISTRATION_PASSWORD_INPUT = Target
            .the("Registration password input")
            .locatedBy("//*[@id=\"form\"]/label[2]/input");

    public static final Target REQISTRATION_DUPLICATE_PASSWORD_INPUT = Target
            .the("Reqistration password duplicate input")
            .locatedBy("//*[@id=\"form\"]/label[3]/input");


    public static final Target REQISTRATION_SUBMIT_BUTTON = Target
            .the("Reqistration submit button")
            .locatedBy("//*[@id=\"form\"]/input");

    public static final Target HOME_PAGE_BANNER = Target
            .the("Home page dummie banner")
            .locatedBy("/html/body/div/div/div[5]");

    public static final Target REGISTRATION_ERROR_MESSAGE = Target
            .the("Home page dummie banner")
            .locatedBy("/html/body/div[1]");

    public static final Target INPUT_REQUIRED = Target
            .the("Input required banner")
            .located(By.cssSelector("input:required"));

    public static final Target GENERATED_TODO = Target
            .the("Generated todo")
            .locatedBy("//*[@id=\"myUL\"]/li");

    public static final Target LOGIN_ERR0R_MESSAGE = Target
            .the("Login error div")
            .locatedBy("/html/body/div[1]");

    public static final Target TODO_ERROR_MESSAGE = Target
            .the("Todo error message")
            .locatedBy("/html/body/div[3]");

    public static final Target LOGIN_FORM = Target
            .the("Login Form")
            .locatedBy("/html/body/div/div/div[1]");

    public static final Target LOGOUT_BUTTON = Target
            .the("Logout button")
            .locatedBy("//*[@id=\"navbarTogglerDemo02\"]/ul/li[2]/div/a[2]");



}
