package zks;

import cz.cvut.fel.still.sqa.seleniumStarterPack.config.DriverFactory;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import zks.tasks.*;

import java.io.IOException;
import java.util.Objects;

import static org.hamcrest.Matchers.hasItem;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.junit.Assert.assertTrue;
import static zks.pageObjects.DonoteHomePage.INPUT_REQUIRED;
import static zks.questions.DonoteHomePage.theDisplayedDonoteHomePage;
import static zks.questions.GeneratedTodo.theDisplayedGeneratedTodo;
import static zks.questions.LoginForm.theDisplayLoginForm;
import static zks.questions.RegistrationErrorMessage.theDisplayRegistrationErrorMessage;


@RunWith(SerenityRunner.class)
public class DonoteTest {

    private WebDriver browser;
    Actor anton  = Actor.named("Anton");

    @Before
    public void before() throws IOException {
        browser = new DriverFactory().getDriver();
        givenThat(anton).can(BrowseTheWeb.with(browser));
    }

    @After
    public void closeBrowser() {
        browser.close();
    }

//    @Test
//    public void createNewUserTest(){
//        givenThat(anton).wasAbleTo(StartsWith.donoteHomePage());
//        when(anton).attemptsTo(CreateNewUserAccount.called());
//        then(anton).should(seeThat(theDisplayedDonoteHomePage(), Objects::nonNull));
//    }

    @Test
    public void createNewEmptyUserTest(){
        givenThat(anton).wasAbleTo(StartsWith.donoteHomePage());
        when(anton).attemptsTo(CreateEmptyUser.called());
        assertTrue(INPUT_REQUIRED.resolveFor(anton).isVisible());
    }

    @Test
    public void createNewEmptyPasswordsUserTest(){
        givenThat(anton).wasAbleTo(StartsWith.donoteHomePage());
        when(anton).attemptsTo(CreateUserWithEmptyPasswords.called());
        assertTrue(INPUT_REQUIRED.resolveFor(anton).isVisible());
    }

    @Test
    public void createNewEmptyOnePasswordUserTest(){
        givenThat(anton).wasAbleTo(StartsWith.donoteHomePage());
        when(anton).attemptsTo(CreateNewUserWithOneEmptyPassword.called());
        assertTrue(INPUT_REQUIRED.resolveFor(anton).isVisible());
    }

    @Test
    public void createNewUserWhithPasswordShorterThan6(){
        String error = "Length of password is shorter then 6 chars";

        givenThat(anton).wasAbleTo(StartsWith.donoteHomePage());
        when(anton).attemptsTo(CreateUserWithPasswordShoterThan6.called());
        then(anton).should(seeThat(theDisplayRegistrationErrorMessage(),hasItem(error)));
    }

    @Test
    public void createNewUserWithoutNumbersInPassword(){
        String error = "Password didnt contain numbers";

        givenThat(anton).wasAbleTo(StartsWith.donoteHomePage());
        when(anton).attemptsTo(CreateNewUserWithoutNumbersInPassword.called());
        then(anton).should(seeThat(theDisplayRegistrationErrorMessage(),hasItem(error)));
    }

    @Test
    public void loginAndCreateNewTodo(){
        String todo = "New Generated Todo\n×";

        givenThat(anton).wasAbleTo(StartsWith.donoteHomePage());
        when(anton).attemptsTo(LoginAndLeaveTheTodo.called());
        then(anton).should(seeThat(theDisplayedGeneratedTodo(),hasItem(todo)));
    }

    @Test
    public void tdl1(){
        //2-4-5-6-9
        givenThat(anton).wasAbleTo(StartsWith.donoteHomePage());
        when(anton).attemptsTo(Tdl1.called());
        then(anton).should(seeThat(theDisplayLoginForm(),hasItem("Login\n\nPassword\n\nRegistration")));
    }

    @Test
    public void tdl2(){
        //2-4-5-11
        givenThat(anton).wasAbleTo(StartsWith.donoteHomePage());
        when(anton).attemptsTo(LoginOpenTodoAndLogout.called());
        then(anton).should(seeThat(theDisplayLoginForm(),hasItem("Login\n\nPassword\n\nRegistration")));
    }

    @Test
    public void tdl3(){
        //2-4-12
        givenThat(anton).wasAbleTo(StartsWith.donoteHomePage());
        when(anton).attemptsTo(Tdl3.called());
        then(anton).should(seeThat(theDisplayLoginForm(),hasItem("Login\n\nPassword\n\nRegistration")));
    }

}
