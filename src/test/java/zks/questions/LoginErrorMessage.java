package zks.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static zks.pageObjects.DonoteHomePage.LOGIN_ERR0R_MESSAGE;

public class LoginErrorMessage implements Question<List<String>> {
    public static Question<List<String>> theDisplayLoginErrorMessage() {
        return new LoginErrorMessage();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(LOGIN_ERR0R_MESSAGE).viewedBy(actor).asList();
    }
}
