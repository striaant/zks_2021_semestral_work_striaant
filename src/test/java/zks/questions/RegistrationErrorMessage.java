package zks.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static zks.pageObjects.DonoteHomePage.REGISTRATION_ERROR_MESSAGE;

public class RegistrationErrorMessage implements Question<List<String>> {

    public static Question<List<String>> theDisplayRegistrationErrorMessage() {
        return new RegistrationErrorMessage();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(REGISTRATION_ERROR_MESSAGE).viewedBy(actor).asList();
    }
}
