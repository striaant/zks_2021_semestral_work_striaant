package zks.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static zks.pageObjects.DonoteHomePage.*;

public class LoginForm implements Question<List<String>> {

    public static Question<List<String>> theDisplayLoginForm() {
        return new LoginForm();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(LOGIN_FORM).viewedBy(actor).asList();
    }
}
