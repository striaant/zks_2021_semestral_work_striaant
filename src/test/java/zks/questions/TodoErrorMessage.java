package zks.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static zks.pageObjects.DonoteHomePage.TODO_ERROR_MESSAGE;

public class TodoErrorMessage implements Question<List<String>>{
    public static Question<List<String>> theDisplayTodoErrorMessage() {
        return new TodoErrorMessage();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(TODO_ERROR_MESSAGE).viewedBy(actor).asList();
    }
}
