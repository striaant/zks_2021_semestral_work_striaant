package zks.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static zks.pageObjects.DonoteHomePage.HOME_PAGE_BANNER;

public class DonoteHomePage implements Question<List<String>> {

    public static Question<List<String>> theDisplayedDonoteHomePage() {
        return new DonoteHomePage();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(HOME_PAGE_BANNER).viewedBy(actor).asList();
    }
}
