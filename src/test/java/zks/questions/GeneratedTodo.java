package zks.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import java.util.List;

import static zks.pageObjects.DonoteHomePage.GENERATED_TODO;


public class GeneratedTodo implements Question<List<String>> {

    public static Question<List<String>> theDisplayedGeneratedTodo() {
        return new GeneratedTodo();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(GENERATED_TODO).viewedBy(actor).asList();
    }
}
